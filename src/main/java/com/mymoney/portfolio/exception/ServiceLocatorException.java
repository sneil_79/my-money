package com.mymoney.portfolio.exception;

public class ServiceLocatorException extends RuntimeException {
  public ServiceLocatorException(String message, Throwable cause) {
    super(message, cause);
  }
}
