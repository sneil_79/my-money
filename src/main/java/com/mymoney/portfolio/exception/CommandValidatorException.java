package com.mymoney.portfolio.exception;

public class CommandValidatorException extends RuntimeException {
  public CommandValidatorException(String commandString, String errMsg, String usage) {
    super(String.format("Invalid command => %s\n%s\nUsage: %s", commandString, errMsg, usage));
  }

  public CommandValidatorException(String message) {
    super(message);
  }
}
