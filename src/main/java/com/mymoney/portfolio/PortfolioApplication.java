package com.mymoney.portfolio;

import com.mymoney.portfolio.command.builder.CommandBuilderHelper;
import com.mymoney.portfolio.command.processor.CommandProcessorHelper;
import com.mymoney.portfolio.model.Command;
import com.mymoney.portfolio.service.CommandFileReader;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
@EnableJpaRepositories("com.mymoney.portfolio.entity")
public class PortfolioApplication implements CommandLineRunner {

  private final CommandFileReader commandFileReader;
  private final CommandBuilderHelper commandBuilderHelper;
  private final CommandProcessorHelper commandProcessorHelper;

  protected final StringBuilder outputResult;

  public static void main(String[] args) {
    ApplicationContext context = SpringApplication.run(PortfolioApplication.class, args);
    int exitStatus = SpringApplication.exit(context, () -> 0);
    System.exit(exitStatus);
  }

  @Override
  public void run(String... args) {
    log.info("Begin processing ...");
    try {

      if (args.length == 0) {
        throw new RuntimeException("No input file provided");
      }

      // validate input file
      log.info("Loading command file " + args[0]);
      List<String> commandStrings = commandFileReader.readCommandFile(args[0]);

      // build list of Command objects for processing
      log.info("Building commands");
      Map<String, List<Command>> commandMap = commandBuilderHelper.buildCommands(commandStrings);

      // process commands
      log.info("Processing commands");
      outputResult.append(commandProcessorHelper.processCommands(commandMap));

    } catch (Exception ex) {
      outputResult.append("Execution failed. Reason: " + ex.getMessage());
    }
    log.info("Done. Printing Output ...");
    writeOutput(outputResult.deleteCharAt(outputResult.length()-1).toString());
  }

  public void writeOutput(String outputResult) {
    System.out.println("********************** OUTPUT **********************");
    System.out.println(outputResult);
    System.out.println("****************************************************");
  }
}
