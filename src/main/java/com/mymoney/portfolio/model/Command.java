package com.mymoney.portfolio.model;

public interface Command extends Comparable<Command> {
  public abstract String getType();
}
