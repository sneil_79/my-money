package com.mymoney.portfolio.model;

import static com.mymoney.portfolio.common.Constants.COMMAND_BALANCE;
import static com.mymoney.portfolio.common.Constants.COMMAND_SIP;

import lombok.Data;

@Data
public class BalanceCommand implements Command {
  public static final String FORMAT = "<FULL_MONTH_NAME>";
  private final String month;

  @Override
  public String getType() {
    return COMMAND_BALANCE;
  }

  @Override
  public String toString() {
    return String.format("%s %s", COMMAND_BALANCE, month);
  }

  @Override
  public int compareTo(Command o) {
    return (o instanceof RebalanceCommand)?-1:1 ;
  }
}
