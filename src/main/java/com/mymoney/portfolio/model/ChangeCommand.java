package com.mymoney.portfolio.model;

import static com.mymoney.portfolio.common.Constants.COMMAND_CHANGE;
import static com.mymoney.portfolio.common.Constants.COMMAND_SIP;

import lombok.Data;

@Data
public class ChangeCommand implements Command {
  public static final String FORMAT =
      "<AMOUNT_EQUITY> <AMOUNT_DEBT> <AMOUNT_GOLD> <FULL_MONTH_NAME>";
  private final Double equityAmount;
  private final Double debtAmount;
  private final Double goldAmount;
  private final String month;

  @Override
  public String getType() {
    return COMMAND_CHANGE;
  }

  @Override
  public String toString() {
    return String.format(
        "%s %.2f %.2f %.2f %s", COMMAND_CHANGE, equityAmount, debtAmount, goldAmount, month);
  }

  @Override
  public int compareTo(Command o) {
    return (o instanceof ContributionCommand)?1:-1 ;
  }
}
