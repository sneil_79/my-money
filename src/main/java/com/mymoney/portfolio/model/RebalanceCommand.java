package com.mymoney.portfolio.model;

import static com.mymoney.portfolio.common.Constants.COMMAND_REBALANCE;
import static com.mymoney.portfolio.common.Constants.COMMAND_SIP;

import lombok.Data;

@Data
public class RebalanceCommand implements Command {
  @Override
  public String getType() {
    return COMMAND_REBALANCE;
  }

  @Override
  public String toString() {
    return COMMAND_REBALANCE;
  }

  @Override
  public int compareTo(Command o) {
    return 1;
  }
}
