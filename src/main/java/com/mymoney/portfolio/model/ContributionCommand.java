package com.mymoney.portfolio.model;

import static com.mymoney.portfolio.common.Constants.COMMAND_SIP;
import static com.mymoney.portfolio.common.Constants.CONTRIBUTION;

import lombok.Data;

@Data
public class ContributionCommand implements Command {
  public static final String FORMAT = "<AMOUNT_EQUITY> <AMOUNT_DEBT> <AMOUNT_GOLD>";
  private final String contributionType;
  private final Long equityAmount;
  private final Long debtAmount;
  private final Long goldAmount;

  @Override
  public String getType() {
    return CONTRIBUTION;
  }

  @Override
  public String toString() {
    return String.format(
        "%s %d %d %d", contributionType, equityAmount, debtAmount, goldAmount);
  }

  @Override
  public int compareTo(Command o) {
    return (o instanceof ContributionCommand && this.contributionType.equals(COMMAND_SIP))?1:-1 ;
  }
}
