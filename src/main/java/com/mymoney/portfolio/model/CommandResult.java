package com.mymoney.portfolio.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class CommandResult {

  private final StatusEnum status;
  private final Object data;
  private String output = StringUtils.EMPTY;

  public enum StatusEnum {
    SUCCESS("SUCCESS"),
    FAILURE("FAILURE");
    private final String value;

    StatusEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
  }
}
