package com.mymoney.portfolio.command.processor;

import com.mymoney.portfolio.model.CommandResult;

import java.util.List;

public interface CommandProcessor<T> {
  public abstract CommandResult process(List<T> commandList);

  public abstract String getType();
}
