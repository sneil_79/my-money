package com.mymoney.portfolio.command.processor;

import static com.mymoney.portfolio.common.Constants.*;
import static com.mymoney.portfolio.model.CommandResult.StatusEnum.SUCCESS;

import com.mymoney.portfolio.entity.Fund;
import com.mymoney.portfolio.entity.FundRepository;
import com.mymoney.portfolio.model.CommandResult;
import com.mymoney.portfolio.model.ContributionCommand;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.util.Precision;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class ContributionCommandProcessor implements CommandProcessor<ContributionCommand> {

  private final FundRepository fundRepository;

  @Override
  public CommandResult process(List<ContributionCommand> commandList) {
    List<Float> contributionPercentages = getAllocationPercentages(commandList.get(0));
    List<Fund> fundList = new ArrayList<>();
    fundList.add(
        new Fund(
            EQUITY,
            commandList.get(0).getEquityAmount(),
            contributionPercentages.get(0),
            commandList.get(1).getEquityAmount()));
    fundList.add(
        new Fund(
            DEBT,
            commandList.get(0).getDebtAmount(),
            contributionPercentages.get(1),
            commandList.get(1).getDebtAmount()));
    fundList.add(
        new Fund(
            GOLD,
            commandList.get(0).getGoldAmount(),
            contributionPercentages.get(2),
            commandList.get(1).getGoldAmount()));

    Iterator<Fund> fundIterator = fundRepository.saveAll(fundList).iterator();
    fundList.clear();
    fundIterator.forEachRemaining(
        fund -> {
          fundList.add(fund);
        });

    log.info("Funds allocated:");
    for (Fund fund : fundList) {
      log.debug("\t" + fund);
    }

    return new CommandResult(SUCCESS, fundList);
  }

  @Override
  public String getType() {
    return CONTRIBUTION;
  }

  private List<Float> getAllocationPercentages(ContributionCommand contributionCommand) {
    Long total =
        contributionCommand.getEquityAmount()
            + contributionCommand.getDebtAmount()
            + contributionCommand.getGoldAmount();
    return Arrays.asList(
        Precision.round((contributionCommand.getEquityAmount() * 100 / total), 2),
        Precision.round(contributionCommand.getDebtAmount() * 100 / total, 2),
        Precision.round(contributionCommand.getGoldAmount() * 100 / total, 2));
  }
}
