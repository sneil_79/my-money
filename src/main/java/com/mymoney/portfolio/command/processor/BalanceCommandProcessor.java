package com.mymoney.portfolio.command.processor;

import static com.mymoney.portfolio.common.Constants.COMMAND_BALANCE;
import static com.mymoney.portfolio.model.CommandResult.StatusEnum.SUCCESS;

import com.mymoney.portfolio.entity.Balance;
import com.mymoney.portfolio.entity.BalanceRepository;
import com.mymoney.portfolio.model.BalanceCommand;
import com.mymoney.portfolio.model.CommandResult;
import java.util.*;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BalanceCommandProcessor implements CommandProcessor<BalanceCommand> {
  private final BalanceRepository balanceRepository;

  @Override
  public CommandResult process(List<BalanceCommand> commandList) {
    Map<String, List<Balance>> balances = new LinkedHashMap<>();
    for (BalanceCommand balanceCommand : commandList) {
      balances.put(
          balanceCommand.getMonth(),
          balanceRepository.findByBalanceMonth(balanceCommand.getMonth()));
    }
    StringBuilder balancesOutput = new StringBuilder();
    for (Map.Entry<String, List<Balance>> monthBalances : balances.entrySet()) {
      balancesOutput.append(
          monthBalances.getValue().stream()
                  .map(balance -> balance.getBalanceAmount())
                  .collect(Collectors.toList())
              + System.lineSeparator());
    }
    return new CommandResult(SUCCESS, balances, balancesOutput.toString());
  }

  @Override
  public String getType() {
    return COMMAND_BALANCE;
  }
}
