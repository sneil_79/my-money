package com.mymoney.portfolio.command.processor;

import static com.mymoney.portfolio.common.Constants.COMMAND_REBALANCE;
import static com.mymoney.portfolio.common.Constants.MSG_CANNOT_REBALANCE;
import static com.mymoney.portfolio.model.CommandResult.StatusEnum.SUCCESS;

import com.mymoney.portfolio.entity.Rebalance;
import com.mymoney.portfolio.entity.RebalanceRepository;
import com.mymoney.portfolio.model.CommandResult;
import com.mymoney.portfolio.model.RebalanceCommand;
import java.time.Month;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RebalanceCommandProcessor implements CommandProcessor<RebalanceCommand> {

  private final RebalanceRepository rebalanceRepository;

  @Override
  public CommandResult process(List<RebalanceCommand> commandList) {
    StringBuilder rebalancesOutput = new StringBuilder();
    Map<String, List<Rebalance>> rebalances = new LinkedHashMap<>();
    List<Rebalance> monthRebalances = null;
    for (Rebalance rebalance : rebalanceRepository.findAll()) {
      monthRebalances = rebalances.get(rebalance.getRebalanceMonth());
      if (monthRebalances == null) {
        monthRebalances = new ArrayList<>();
        rebalances.put(rebalance.getRebalanceMonth(), monthRebalances);
      }
      monthRebalances.add(rebalance);
    }
    if (rebalances.isEmpty()) {
      rebalancesOutput.append(MSG_CANNOT_REBALANCE);
    } else {
      monthRebalances =
          rebalances.containsKey(Month.DECEMBER.toString())
              ? rebalances.get(Month.DECEMBER.toString())
              : rebalances.get(Month.JUNE.toString());

      rebalancesOutput.append(
          monthRebalances.stream()
                  .map(rebalance -> rebalance.getRebalanceAmount())
                  .collect(Collectors.toList())
              + System.lineSeparator());
    }
    return new CommandResult(SUCCESS, monthRebalances, rebalancesOutput.toString());
  }

  @Override
  public String getType() {
    return COMMAND_REBALANCE;
  }
}
