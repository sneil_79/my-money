package com.mymoney.portfolio.command.processor;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CommandProcessorFactory {
  private final Map<String, CommandProcessor> commandProcessorMap;

  @Autowired
  private CommandProcessorFactory(List<CommandProcessor> commandProcessors) {
    commandProcessorMap =
        commandProcessors.stream()
            .collect(Collectors.toUnmodifiableMap(CommandProcessor::getType, Function.identity()));
  }

  public CommandProcessor getCommandProcessor(String type) {
    return Optional.ofNullable(commandProcessorMap.get(type))
        .orElseThrow(IllegalArgumentException::new);
  }
}
