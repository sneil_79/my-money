package com.mymoney.portfolio.command.processor;

import static com.mymoney.portfolio.common.Constants.*;
import static com.mymoney.portfolio.model.CommandResult.StatusEnum.FAILURE;
import static com.mymoney.portfolio.model.CommandResult.StatusEnum.SUCCESS;

import com.mymoney.portfolio.entity.*;
import com.mymoney.portfolio.exception.CommandValidatorException;
import com.mymoney.portfolio.model.ChangeCommand;
import com.mymoney.portfolio.model.CommandResult;
import java.time.Month;
import java.util.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ChangeCommandProcessor implements CommandProcessor<ChangeCommand> {
  private final BalanceRepository balanceRepository;
  private final RebalanceRepository rebalanceRepository;
  private final FundRepository fundRepository;

  @Override
  public CommandResult process(List<ChangeCommand> commandList) {

    ChangeCommand changeJan = commandList.get(0);
    if (!changeJan.getMonth().equals(Month.JANUARY.toString())) {
      return new CommandResult(FAILURE, null);
    }

    Map<String, List<Balance>> allBalances = new LinkedHashMap<>();
    for (ChangeCommand changeCommand : commandList) {
      List<Balance> balanceList = new ArrayList<>();
      balanceList.add(
          calculateBalance(EQUITY, changeCommand.getMonth(), changeCommand.getEquityAmount()));
      balanceList.add(
          calculateBalance(DEBT, changeCommand.getMonth(), changeCommand.getDebtAmount()));
      balanceList.add(
          calculateBalance(GOLD, changeCommand.getMonth(), changeCommand.getGoldAmount()));

      Iterator<Balance> balanceIterator = balanceRepository.saveAll(balanceList).iterator();
      balanceList.clear();
      balanceIterator.forEachRemaining(
          balance -> {
            balanceList.add(balance);
          });
      log.info("Saved balances for " + changeCommand.getMonth());
      allBalances.put(changeCommand.getMonth(), balanceList);

      if (changeCommand.getMonth().equals(Month.JUNE.toString())
          || changeCommand.getMonth().equals(Month.DECEMBER.toString())) {
        List<Rebalance> rebalanceList = calculateRebalances(balanceList);
        rebalanceRepository.saveAll(rebalanceList);
        log.info("Saved rebalances for " + changeCommand.getMonth());
      }
    }

    return new CommandResult(SUCCESS, allBalances);
  }

  protected Balance calculateBalance(String fundName, String month, Double changePercentage) {
    Balance balance = new Balance();
    Fund fund = fundRepository.findByFundName(fundName);
    Long currentBalance;
    balance.setFund(fund);
    balance.setBalanceMonth(month);
    if (month.equals(Month.JANUARY.toString())) {
      currentBalance = fund.getAllocationAmount();
    } else {
      String previousMonth = Month.valueOf(month).minus(1).toString();
      Balance currBal = balanceRepository.findByBalanceMonthAndFund(previousMonth, fund);
      if (currBal == null) {
        throw new CommandValidatorException("Missing CHANGE command for month " + previousMonth);
      } else {
        currentBalance = currBal.getBalanceAmount() + fund.getSipAmount();
      }
    }
    balance.setBalanceAmount(getNewBalance(currentBalance, changePercentage));
    return balance;
  }

  protected List<Rebalance> calculateRebalances(List<Balance> balanceList) {
    List<Rebalance> rebalanceList = new ArrayList<>();
    Long totalBalance = balanceList.stream().mapToLong(bal -> bal.getBalanceAmount()).sum();
    for (Balance balance : balanceList) {
      rebalanceList.add(
          new Rebalance(
              balance.getFund(),
              balance.getBalanceMonth(),
              Math.round(
                  Math.floor((totalBalance * balance.getFund().getAllocationPercentage() / 100)))));
    }
    return rebalanceList;
  }

  private Long getNewBalance(Long currentBalance, Double changePercentage) {
    return Math.round(Math.floor((currentBalance + (changePercentage * currentBalance / 100))));
  }

  @Override
  public String getType() {
    return COMMAND_CHANGE;
  }
}
