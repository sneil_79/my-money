package com.mymoney.portfolio.command.processor;

import static com.mymoney.portfolio.model.CommandResult.StatusEnum.SUCCESS;

import com.mymoney.portfolio.model.Command;
import java.util.List;
import java.util.Map;

import com.mymoney.portfolio.model.CommandResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CommandProcessorHelper {
  private final CommandProcessorFactory commandProcessorFactory;

  public String processCommands(Map<String, List<Command>> commandMap) {
    StringBuilder output = new StringBuilder();
    log.debug("Command sets to be executed: " + commandMap.keySet());
    for (Map.Entry<String, List<Command>> commandMapEntry : commandMap.entrySet()) {
      // call factory method to return command-specific processor
      CommandProcessor commandProcessor =
          commandProcessorFactory.getCommandProcessor(commandMapEntry.getKey());
      log.debug("Processing command set " + commandMapEntry.getValue());
      output.append(commandProcessor.process(commandMapEntry.getValue()).getOutput());
    }
    return output.toString();
  }
}
