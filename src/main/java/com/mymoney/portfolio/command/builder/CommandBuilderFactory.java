package com.mymoney.portfolio.command.builder;

public interface CommandBuilderFactory {
  /**
   * Factory method that retrieves an instance of CommandBuilder based on the command name
   * provided *
   */
  CommandBuilder getCommandBuilder(String commandName);
}
