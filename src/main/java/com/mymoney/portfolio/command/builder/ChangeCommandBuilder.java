package com.mymoney.portfolio.command.builder;

import static com.mymoney.portfolio.common.Constants.*;
import static com.mymoney.portfolio.common.Utils.isValidMonth;
import static com.mymoney.portfolio.model.ChangeCommand.FORMAT;

import com.mymoney.portfolio.exception.CommandValidatorException;
import com.mymoney.portfolio.model.ChangeCommand;
import com.mymoney.portfolio.model.Command;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

public class ChangeCommandBuilder implements CommandBuilder {
  static final String INVALID_LOSS_PERCENTAGE = "Loss percentage cannot exceed 100%";

  @Override
  public Command build(String commandString) throws CommandValidatorException {
    String[] commandParts = commandString.split(StringUtils.SPACE);
    String monthName = commandParts[commandParts.length - 1];

    List<Double> contributionAmounts =
        Arrays.asList(commandParts).stream()
            .filter(item -> NumberUtils.isParsable(item.substring(0, item.length() - 1)))
            .map(
                item -> {
                  Double amount;
                  amount = Double.valueOf(item.substring(0, item.length() - 1));
                  if (amount < -100) {
                    throw new CommandValidatorException(
                        commandString, INVALID_LOSS_PERCENTAGE, FORMAT);
                  }
                  return amount;
                })
            .collect(Collectors.toList());
    if (contributionAmounts.size() != 3 || !isValidMonth(monthName)) {
      throw new CommandValidatorException(commandString, INVALID_PARAMETER, FORMAT);
    }

    return new ChangeCommand(
        contributionAmounts.get(0),
        contributionAmounts.get(1),
        contributionAmounts.get(2),
        monthName);
  }
}
