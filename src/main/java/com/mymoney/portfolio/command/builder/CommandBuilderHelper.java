package com.mymoney.portfolio.command.builder;

import static com.mymoney.portfolio.common.PortfolioConfig.BUILDER_KEY;

import com.mymoney.portfolio.exception.CommandValidatorException;
import com.mymoney.portfolio.exception.ServiceLocatorException;
import com.mymoney.portfolio.model.Command;
import java.util.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CommandBuilderHelper {

  private final CommandBuilderFactory commandBuilderFactory;

  public Map<String, List<Command>> buildCommands(List<String> commandStrings) {
    Map<String, List<Command>> commandMap = new HashMap<>();
    for (String commandString : commandStrings) {
      // call factory method to return command-specific builder
      try {
        CommandBuilder commandBuilder =
            commandBuilderFactory.getCommandBuilder(
                commandString.split(StringUtils.SPACE)[0] + BUILDER_KEY);
        log.debug("Building command - " + commandString);
        Command command = commandBuilder.build(commandString);
        List<Command> commands = commandMap.get(command.getType());
        if (commands == null) {
          commands = new ArrayList<>();
          commandMap.put(command.getType(), commands);
        }
        commands.add(command);
      } catch (ServiceLocatorException ex) {
        throw new CommandValidatorException("Invalid command format " + commandString);
      }
    }

    List<Map.Entry<String, List<Command>>> list = new ArrayList<>(commandMap.entrySet());
    Collections.sort(list, Comparator.comparing(obj -> obj.getValue().get(0)));
    Map<String, List<Command>> resultMap = new LinkedHashMap<>();
    list.forEach(obj -> resultMap.put(obj.getKey(), obj.getValue()));
    return resultMap;
  }
}
