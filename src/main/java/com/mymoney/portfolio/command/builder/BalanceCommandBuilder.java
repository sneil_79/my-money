package com.mymoney.portfolio.command.builder;

import static com.mymoney.portfolio.common.Constants.INVALID_PARAMETER;
import static com.mymoney.portfolio.common.Utils.isValidMonth;
import static com.mymoney.portfolio.model.BalanceCommand.FORMAT;

import com.mymoney.portfolio.exception.CommandValidatorException;
import com.mymoney.portfolio.model.BalanceCommand;
import com.mymoney.portfolio.model.Command;
import org.apache.commons.lang3.StringUtils;

public class BalanceCommandBuilder implements CommandBuilder {
  @Override
  public Command build(String commandString) throws CommandValidatorException {
    String[] commandParts = commandString.split(StringUtils.SPACE);
    if (commandParts.length != 2 || !isValidMonth(commandParts[1])) {
      throw new CommandValidatorException(commandString, INVALID_PARAMETER, FORMAT);
    }
    return new BalanceCommand(commandParts[1]);
  }
}
