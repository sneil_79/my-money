package com.mymoney.portfolio.command.builder;

import com.mymoney.portfolio.exception.CommandValidatorException;
import com.mymoney.portfolio.model.Command;

public interface CommandBuilder {
  Command build(String commandString) throws CommandValidatorException;
}
