package com.mymoney.portfolio.command.builder;

import com.mymoney.portfolio.model.Command;
import com.mymoney.portfolio.model.RebalanceCommand;

public class RebalanceCommandBuilder implements CommandBuilder {
  @Override
  public Command build(String commandString) {
    return new RebalanceCommand();
  }
}
