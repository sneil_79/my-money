package com.mymoney.portfolio.command.builder;

import static com.mymoney.portfolio.common.Constants.*;
import static com.mymoney.portfolio.model.ContributionCommand.FORMAT;

import com.mymoney.portfolio.exception.CommandValidatorException;
import com.mymoney.portfolio.model.Command;
import com.mymoney.portfolio.model.ContributionCommand;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

public class ContributionCommandBuilder implements CommandBuilder {

  @Override
  public Command build(String commandString) throws CommandValidatorException {
    String[] commandParts = commandString.split(StringUtils.SPACE);
    String contributionType = commandParts[0];

    List<Long> contributionAmounts =
        Arrays.asList(commandParts).stream()
            .filter(item -> NumberUtils.isDigits(item))
            .map(
                item -> {
                  Long amount;
                  amount = Long.valueOf(item);
                  return amount;
                })
            .collect(Collectors.toList());
    if (contributionAmounts.size() != 3) {
      throw new CommandValidatorException(commandString, INVALID_PARAMETER, FORMAT);
    }

    return new ContributionCommand(
        contributionType,
        contributionAmounts.get(0),
        contributionAmounts.get(1),
        contributionAmounts.get(2));
  }
}
