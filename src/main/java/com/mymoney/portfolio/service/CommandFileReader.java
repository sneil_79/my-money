package com.mymoney.portfolio.service;

import static com.mymoney.portfolio.common.Constants.*;

import com.mymoney.portfolio.exception.FileValidationException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CommandFileReader {

  public static final String FILE_DOES_NOT_EXIST = "Command input file %s does not exist";
  public static final String INCORRECT_COMMAND_SEQUENCE =
      "Incorrect command sequence in input file. [Expected: ALLOCATE,SIP,CHANGE,BALANCE,REBALANCE]";
  public static final String FILE_READ_ERROR = "Error reading command file %s";

  public List<String> readCommandFile(String commandFilename) throws FileValidationException {
    try {
      Path path = Paths.get(commandFilename);
      if (!fileExists(path)) {
        String invalidMessage = String.format(FILE_DOES_NOT_EXIST, commandFilename);
        log.error(invalidMessage);
        throw new FileValidationException(invalidMessage);
      }
      List<String> commandStrings = Files.readAllLines(path);
      if (!checkFileFormat(commandStrings)) {
        log.error(INCORRECT_COMMAND_SEQUENCE);
        throw new FileValidationException(INCORRECT_COMMAND_SEQUENCE);
      }
      return commandStrings;
    } catch (IOException ex) {
      throw new FileValidationException(String.format(FILE_READ_ERROR, ex.getMessage()));
    }
  }

  protected boolean fileExists(Path path) {
    // return false if file doesn't exist or is not .txt
    if (Files.exists(path)
        && FilenameUtils.getExtension(path.getFileName().toString())
            .equalsIgnoreCase(FILETYPE_TXT)) {
      log.debug("File exists check passed.");
      return true;
    }
    return false;
  }

  protected boolean checkFileFormat(List<String> fileContent) {
    // check command sequence, duplicate commands, missing commands, unsupported commands
    String previousCommand = StringUtils.EMPTY;
    for (String line : fileContent) {
      if (!line.startsWith(COMMAND_ALLOCATE)
          && !line.startsWith(COMMAND_SIP)
          && !line.startsWith(COMMAND_CHANGE)
          && !line.startsWith(COMMAND_BALANCE)
          && !line.startsWith(COMMAND_REBALANCE)) {
        return false;
      } else {
        if (line.startsWith(COMMAND_ALLOCATE) && !previousCommand.isEmpty()) {
          return false;
        } else if (line.startsWith(COMMAND_SIP) && !previousCommand.startsWith(COMMAND_ALLOCATE)) {
          return false;
        } else if (line.startsWith(COMMAND_CHANGE)
            && !(previousCommand.startsWith(COMMAND_SIP)
                || previousCommand.startsWith(COMMAND_CHANGE))) {
          return false;
        } else if (line.startsWith(COMMAND_BALANCE)
            && !(previousCommand.startsWith(COMMAND_CHANGE)
                || previousCommand.startsWith(COMMAND_BALANCE))) {
          return false;
        } else if (line.startsWith(COMMAND_REBALANCE)
            && !previousCommand.startsWith(COMMAND_BALANCE)) {
          return false;
        }
      }
      previousCommand = line;
    }
    if (!previousCommand.startsWith(COMMAND_REBALANCE)) {
      return false;
    }
    log.debug("File format check passed.");
    return true;
  }
}
