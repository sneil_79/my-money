package com.mymoney.portfolio.entity;

import javax.persistence.*;
import lombok.*;

@Entity
@Table(name = "Balance")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Balance {
  @Id
  @Column(name = "balanceId")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long balanceId;

  @OneToOne
  @JoinColumn(name = "fundId", nullable = false)
  private Fund fund;

  private String balanceMonth;

  private Long balanceAmount;

  public Balance(Fund fund, String balanceMonth, Long balanceAmount) {
    this.fund = fund;
    this.balanceMonth = balanceMonth;
    this.balanceAmount = balanceAmount;
  }
}
