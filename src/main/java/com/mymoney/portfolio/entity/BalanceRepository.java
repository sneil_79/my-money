package com.mymoney.portfolio.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BalanceRepository extends CrudRepository<Balance, Long> {
  List<Balance> findByBalanceMonth(String month);

  Balance findByBalanceMonthAndFund(String month, Fund fund);
}
