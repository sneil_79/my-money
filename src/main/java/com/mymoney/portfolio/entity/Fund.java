package com.mymoney.portfolio.entity;

import javax.persistence.*;

import lombok.*;

@Entity
@Table(name = "Fund")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Fund {
  @Id
  @Column(name = "fundId")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long fundId;

  private  String fundName;
  private  Long allocationAmount;
  private  Float allocationPercentage;
  private  Long sipAmount;

  public Fund(String fundName, Long allocationAmount, Float allocationPercentage, Long sipAmount) {
    this.fundName = fundName;
    this.allocationAmount = allocationAmount;
    this.allocationPercentage = allocationPercentage;
    this.sipAmount = sipAmount;
  }
}
