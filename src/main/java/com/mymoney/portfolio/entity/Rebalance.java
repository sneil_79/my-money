package com.mymoney.portfolio.entity;

import javax.persistence.*;
import lombok.*;

@Entity
@Table(name = "Rebalance")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Rebalance {
  @Id
  @Column(name = "rebalanceId")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long rebalanceId;

  @OneToOne
  @JoinColumn(name = "fundId", nullable = false)
  private Fund fund;

  private String rebalanceMonth;

  private Long rebalanceAmount;

  public Rebalance(Fund fund, String rebalanceMonth, Long rebalanceAmount) {
    this.fund = fund;
    this.rebalanceMonth = rebalanceMonth;
    this.rebalanceAmount = rebalanceAmount;
  }
}
