package com.mymoney.portfolio.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FundRepository extends CrudRepository<Fund, Long> {
  Fund findByFundName(String fundName);
}
