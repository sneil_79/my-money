package com.mymoney.portfolio.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RebalanceRepository extends CrudRepository<Rebalance, Long> {}
