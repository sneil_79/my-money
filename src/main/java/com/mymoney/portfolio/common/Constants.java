package com.mymoney.portfolio.common;

public class Constants {
  public static final String COMMAND_ALLOCATE = "ALLOCATE";
  public static final String COMMAND_SIP = "SIP";
  public static final String COMMAND_CHANGE = "CHANGE";
  public static final String COMMAND_BALANCE = "BALANCE";
  public static final String COMMAND_REBALANCE = "REBALANCE";
  public static final String CONTRIBUTION = "CONTRIBUTION";
  public static final String EQUITY = "Equity";
  public static final String DEBT = "Debt";
  public static final String GOLD = "Gold";
  public static final String FILETYPE_TXT = "txt";
  public static final String INVALID_PARAMETER = "Invalid or missing parameters";
  public static final String MSG_CANNOT_REBALANCE = "CANNOT_REBALANCE";
}
