package com.mymoney.portfolio.common;

import java.time.Month;
import java.util.Arrays;

public class Utils {
  public static boolean isValidMonth(String monthName) {
    return Arrays.stream(Month.values())
            .filter(month -> month.toString().equalsIgnoreCase(monthName))
            .count()
        > 0;
  }
}
