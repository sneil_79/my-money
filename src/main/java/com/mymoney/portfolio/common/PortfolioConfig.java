package com.mymoney.portfolio.common;

import static com.mymoney.portfolio.common.Constants.*;

import com.mymoney.portfolio.command.builder.*;
import com.mymoney.portfolio.exception.ServiceLocatorException;
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PortfolioConfig {

  public static final String BUILDER_KEY = ".command.builder";

  @Bean("commandBuilderFactory")
  public ServiceLocatorFactoryBean commandBuilderFactory() {
    ServiceLocatorFactoryBean serviceLocatorFactoryBean = new ServiceLocatorFactoryBean();
    serviceLocatorFactoryBean.setServiceLocatorInterface(CommandBuilderFactory.class);
    serviceLocatorFactoryBean.setServiceLocatorExceptionClass(ServiceLocatorException.class);
    return serviceLocatorFactoryBean;
  }

  @Bean("outputResult")
  public StringBuilder outputResult() {
    return new StringBuilder();
  }

  @Bean(COMMAND_ALLOCATE + BUILDER_KEY)
  public CommandBuilder allocateCommandBuilder() {
    return new ContributionCommandBuilder();
  }

  @Bean(COMMAND_SIP + BUILDER_KEY)
  public CommandBuilder sipCommandBuilder() {
    return new ContributionCommandBuilder();
  }

  @Bean(COMMAND_CHANGE + BUILDER_KEY)
  public CommandBuilder changeCommandBuilder() {
    return new ChangeCommandBuilder();
  }

  @Bean(COMMAND_BALANCE + BUILDER_KEY)
  public CommandBuilder balanceCommandBuilder() {
    return new BalanceCommandBuilder();
  }

  @Bean(COMMAND_REBALANCE + BUILDER_KEY)
  public CommandBuilder rebalanceCommandBuilder() {
    return new RebalanceCommandBuilder();
  }
}
