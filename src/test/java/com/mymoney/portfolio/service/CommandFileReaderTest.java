package com.mymoney.portfolio.service;

import static org.junit.jupiter.api.Assertions.*;

import com.mymoney.portfolio.exception.FileValidationException;
import java.nio.file.Paths;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

class CommandFileReaderTest {

  private CommandFileReader commandFileReader = new CommandFileReader();
  private static String file_that_does_not_exist = "file_that_does_not_exist";
  private static String validCommandFile;
  private static String invalidCommandFile;

  @BeforeAll
  static void setup() throws Exception {
    validCommandFile = new ClassPathResource("command_input.txt").getFile().getAbsolutePath();
    invalidCommandFile =
        new ClassPathResource("command_input_invalid.txt").getFile().getAbsolutePath();
  }

  @Test
  void validateCommandFile() {
    assertNotNull(commandFileReader.readCommandFile(validCommandFile));
    assertThrows(
        FileValidationException.class,
        () -> {
          commandFileReader.readCommandFile(file_that_does_not_exist);
        });
    assertThrows(
        FileValidationException.class,
        () -> {
          commandFileReader.readCommandFile(invalidCommandFile);
        });
  }

  @Test
  void fileExists() throws Exception {
    assertEquals(false, commandFileReader.fileExists(Paths.get(file_that_does_not_exist)));
    assertEquals(true, commandFileReader.fileExists(Paths.get(validCommandFile)));
  }

  @Test
  void checkFileFormat() {
    assertEquals(
        true,
        commandFileReader.checkFileFormat(
            Arrays.asList(
                "ALLOCATE", "SIP", "CHANGE", "CHANGE", "BALANCE", "BALANCE", "REBALANCE")));
    assertEquals(
        false,
        commandFileReader.checkFileFormat(
            Arrays.asList("ALLOCATE", "ALLOCATE", "CHANGE", "BALANCE", "REBALANCE")));
    assertEquals(
        false,
        commandFileReader.checkFileFormat(
            Arrays.asList(
                "ALLOCATE", "SIP", "CHANGE", "CHANGE", "BALANCE", "CHANGE", "REBALANCE")));
    assertEquals(
        false,
        commandFileReader.checkFileFormat(
            Arrays.asList("ALLOCATE", "SIP", "CHANGE", "REBALANCE", "BALANCE", "REBALANCE")));
    assertEquals(
        false,
        commandFileReader.checkFileFormat(Arrays.asList("ALLOCATE", "SIP", "CHANGE", "BALANCE")));
    assertEquals(
        false,
        commandFileReader.checkFileFormat(
            Arrays.asList("ALLOCATE", "BADCOMMAND", "SIP", "CHANGE", "BALANCE", "REBALANCE")));
  }
}
