package com.mymoney.portfolio;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.ClassPathResource;

class PortfolioApplicationTest {

  @Test
  void run() throws Exception {
    ConfigurableApplicationContext ctx =
        SpringApplication.run(
            PortfolioApplication.class,
            new ClassPathResource("command_input.txt").getFile().getAbsolutePath());

    StringBuilder outputResult = (StringBuilder) ctx.getBean("outputResult");
    assertTrue(outputResult.toString().contains("10593, 7897, 2272"));
    assertTrue(outputResult.toString().contains("41818, 23454, 5110"));
    assertTrue(outputResult.toString().contains("48199, 26410, 5329"));
    assertTrue(outputResult.toString().contains("55218, 29602, 5537"));
    assertTrue(outputResult.toString().contains("54214, 27107, 9035"));
  }
}
