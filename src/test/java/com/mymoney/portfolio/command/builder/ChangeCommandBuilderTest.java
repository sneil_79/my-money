package com.mymoney.portfolio.command.builder;

import static com.mymoney.portfolio.command.builder.ChangeCommandBuilder.INVALID_LOSS_PERCENTAGE;
import static com.mymoney.portfolio.common.Constants.*;
import static org.junit.jupiter.api.Assertions.*;

import com.mymoney.portfolio.exception.CommandValidatorException;
import com.mymoney.portfolio.model.ChangeCommand;
import java.time.Month;
import org.junit.jupiter.api.Test;

class ChangeCommandBuilderTest {

  ChangeCommandBuilder builder = new ChangeCommandBuilder();

  @Test
  void build() {
    CommandValidatorException exception =
        assertThrows(
            CommandValidatorException.class,
            () -> {
              builder.build("-101.00% -10% 20.00% MAY");
            });
    assertTrue(exception.getMessage().contains(INVALID_LOSS_PERCENTAGE));

    exception =
        assertThrows(
            CommandValidatorException.class,
            () -> {
              builder.build("1.00% -10% 20.00%");
            });
    assertTrue(exception.getMessage().contains(INVALID_PARAMETER));

    exception =
        assertThrows(
            CommandValidatorException.class,
            () -> {
              builder.build("BAD -10% 20.00% JANUARY");
            });
    assertTrue(exception.getMessage().contains(INVALID_PARAMETER));

    exception =
        assertThrows(
            CommandValidatorException.class,
            () -> {
              builder.build("1.00% 20.00% JUNE");
            });
    assertTrue(exception.getMessage().contains(INVALID_PARAMETER));

    exception =
        assertThrows(
            CommandValidatorException.class,
            () -> {
              builder.build("1.00% -10% 20.00% JAN");
            });
    assertTrue(exception.getMessage().contains(INVALID_PARAMETER));

    ChangeCommand command = (ChangeCommand) builder.build("1.00% -10% 20.00% FEBRUARY");
    assertNotNull(command);
    assertEquals(COMMAND_CHANGE, command.getType());
    assertEquals(Month.FEBRUARY.toString(), command.getMonth());
    assertEquals(-10.00, command.getDebtAmount());
  }
}
