package com.mymoney.portfolio.command.builder;

import static com.mymoney.portfolio.common.Constants.COMMAND_BALANCE;
import static com.mymoney.portfolio.common.Constants.INVALID_PARAMETER;
import static org.junit.jupiter.api.Assertions.*;

import com.mymoney.portfolio.exception.CommandValidatorException;
import com.mymoney.portfolio.model.BalanceCommand;
import java.time.Month;
import org.junit.jupiter.api.Test;

class BalanceCommandBuilderTest {

  BalanceCommandBuilder builder = new BalanceCommandBuilder();

  @Test
  void build() {
    CommandValidatorException exception =
        assertThrows(
            CommandValidatorException.class,
            () -> {
              builder.build("BALANCE");
            });
    assertTrue(exception.getMessage().contains(INVALID_PARAMETER));

    exception =
        assertThrows(
            CommandValidatorException.class,
            () -> {
              builder.build("BALANCE BAD");
            });
    assertTrue(exception.getMessage().contains(INVALID_PARAMETER));

    BalanceCommand command = (BalanceCommand) builder.build("BALANCE MAY");
    assertNotNull(command);
    assertEquals(COMMAND_BALANCE, command.getType());
    assertEquals(Month.MAY.toString(), command.getMonth());
  }
}
