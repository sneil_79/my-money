package com.mymoney.portfolio.command.builder;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RebalanceCommandBuilderTest {

  @Test
  void build() {
    assertNotNull(new RebalanceCommandBuilder().build(StringUtils.EMPTY));
  }
}
