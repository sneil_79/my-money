package com.mymoney.portfolio.command.builder;

import static com.mymoney.portfolio.common.Constants.*;
import static org.junit.jupiter.api.Assertions.*;

import com.mymoney.portfolio.exception.CommandValidatorException;
import com.mymoney.portfolio.model.ContributionCommand;
import org.junit.jupiter.api.Test;

class ContributionCommandBuilderTest {

  ContributionCommandBuilder builder = new ContributionCommandBuilder();

  @Test
  void build() {
    CommandValidatorException exception =
        assertThrows(
            CommandValidatorException.class,
            () -> {
              builder.build("ALLOCATE -10 20 30");
            });
    assertTrue(exception.getMessage().contains(INVALID_PARAMETER));

    exception =
        assertThrows(
            CommandValidatorException.class,
            () -> {
              builder.build("ALLOCATE 10 20");
            });
    assertTrue(exception.getMessage().contains(INVALID_PARAMETER));

    exception =
        assertThrows(
            CommandValidatorException.class,
            () -> {
              builder.build("ALLOCATE 10 20");
            });
    assertTrue(exception.getMessage().contains(INVALID_PARAMETER));

    exception =
        assertThrows(
            CommandValidatorException.class,
            () -> {
              builder.build("ALLOCATE 60.00 20 30");
            });
    assertTrue(exception.getMessage().contains(INVALID_PARAMETER));

    exception =
        assertThrows(
            CommandValidatorException.class,
            () -> {
              builder.build("ALLOCATE 60% 20 10");
            });
    assertTrue(exception.getMessage().contains(INVALID_PARAMETER));

    ContributionCommand command = (ContributionCommand) builder.build("ALLOCATE 60 30 10");
    assertNotNull(command);
    assertEquals(CONTRIBUTION, command.getType());
    assertEquals(COMMAND_ALLOCATE, command.getContributionType());
    assertEquals(30, command.getDebtAmount());

    command = (ContributionCommand) builder.build("SIP 100 27 560");
    assertNotNull(command);
    assertEquals(CONTRIBUTION, command.getType());
    assertEquals(COMMAND_SIP, command.getContributionType());
    assertEquals(560, command.getGoldAmount());
  }
}
