package com.mymoney.portfolio.command.builder;

import static com.mymoney.portfolio.common.Constants.*;
import static com.mymoney.portfolio.common.PortfolioConfig.BUILDER_KEY;
import static org.junit.jupiter.api.Assertions.*;

import com.mymoney.portfolio.exception.CommandValidatorException;
import com.mymoney.portfolio.exception.ServiceLocatorException;
import com.mymoney.portfolio.model.Command;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

class CommandBuilderHelperTest {

  CommandBuilderHelper helper;

  @Test
  void buildCommands() {
    CommandBuilderFactory factory =
        new CommandBuilderFactory() {
          @Override
          public CommandBuilder getCommandBuilder(String commandName) {
            if (commandName.equals(COMMAND_ALLOCATE + BUILDER_KEY)) {
              return new ContributionCommandBuilder();
            } else if (commandName.equals(COMMAND_SIP + BUILDER_KEY)) {
              return new ContributionCommandBuilder();
            } else if (commandName.equals(COMMAND_CHANGE + BUILDER_KEY)) {
              return new ChangeCommandBuilder();
            } else if (commandName.equals(COMMAND_BALANCE + BUILDER_KEY)) {
              return new BalanceCommandBuilder();
            } else if (commandName.equals(COMMAND_REBALANCE + BUILDER_KEY)) {
              return new RebalanceCommandBuilder();
            } else {
              throw new ServiceLocatorException("", null);
            }
          }
        };

    helper = new CommandBuilderHelper(factory);
    Map<String, List<Command>> commandMap =
        helper.buildCommands(
            Arrays.asList(
                "ALLOCATE 8000 6000 3500",
                "SIP 3000 2000 1000",
                "CHANGE 11.00% 9.00% 4.00% JANUARY",
                "CHANGE -6.00% 21.00% -3.00% FEBRUARY",
                "CHANGE 22.00% 21.00% 0.00% MARCH",
                "BALANCE JANUARY",
                "BALANCE FEBRUARY",
                "REBALANCE"));
    assertEquals(4, commandMap.size());
    assertEquals(2, commandMap.get(CONTRIBUTION).size());
    assertEquals(3, commandMap.get(COMMAND_CHANGE).size());
    assertEquals(2, commandMap.get(COMMAND_BALANCE).size());
    assertEquals(1, commandMap.get(COMMAND_REBALANCE).size());

    CommandValidatorException exception =
        assertThrows(
            CommandValidatorException.class,
            () -> {
              helper.buildCommands(Arrays.asList("ALLOCATE8000 6000 3500"));
            });
    assertTrue(exception.getMessage().contains("Invalid command format"));
  }
}
