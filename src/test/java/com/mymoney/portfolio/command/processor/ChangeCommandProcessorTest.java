package com.mymoney.portfolio.command.processor;

import static com.mymoney.portfolio.common.Constants.*;
import static com.mymoney.portfolio.model.CommandResult.StatusEnum.FAILURE;
import static com.mymoney.portfolio.model.CommandResult.StatusEnum.SUCCESS;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.MockitoAnnotations.initMocks;

import com.mymoney.portfolio.entity.*;
import com.mymoney.portfolio.exception.CommandValidatorException;
import com.mymoney.portfolio.model.ChangeCommand;
import com.mymoney.portfolio.model.CommandResult;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

class ChangeCommandProcessorTest {

  @Mock BalanceRepository balanceRepository;
  @Mock RebalanceRepository rebalanceRepository;
  @Mock FundRepository fundRepository;

  Fund equityFund;
  Fund debtFund;
  Fund goldFund;
  Balance equityBalance;
  Balance debtBalance;
  Balance goldBalance;

  @InjectMocks ChangeCommandProcessor processor;

  @BeforeEach
  void init_mocks() {
    initMocks(this);
    equityFund = new Fund(EQUITY, 6000L, 60.0F, 500L);
    debtFund = new Fund(DEBT, 3000L, 30.0F, 1000L);
    goldFund = new Fund(GOLD, 1000L, 10.0F, 250L);
    Mockito.when(fundRepository.findByFundName(EQUITY)).thenReturn(equityFund);
    Mockito.when(fundRepository.findByFundName(DEBT)).thenReturn(debtFund);
    Mockito.when(fundRepository.findByFundName(GOLD)).thenReturn(goldFund);

    equityBalance = processor.calculateBalance(EQUITY, Month.JANUARY.toString(), -10.0);
    debtBalance = processor.calculateBalance(DEBT, Month.JANUARY.toString(), 5.0);
    goldBalance = processor.calculateBalance(GOLD, Month.JANUARY.toString(), 20.0);

    Mockito.when(balanceRepository.findByBalanceMonthAndFund(Month.JANUARY.toString(), equityFund))
        .thenReturn(equityBalance);
    Mockito.when(balanceRepository.findByBalanceMonthAndFund(Month.JANUARY.toString(), debtFund))
        .thenReturn(debtBalance);
    Mockito.when(balanceRepository.findByBalanceMonthAndFund(Month.JANUARY.toString(), goldFund))
        .thenReturn(goldBalance);
  }

  @Test
  void process() {
    List<ChangeCommand> commandList = new ArrayList<>();
    ChangeCommand changeCommand = new ChangeCommand(-10.0, 5.0, 20.0, Month.JANUARY.toString());
    commandList.add(changeCommand);
    changeCommand = new ChangeCommand(15.0, -20.0, 5.0, Month.FEBRUARY.toString());
    commandList.add(changeCommand);
    CommandResult result = processor.process(commandList);
    assertNotNull(result);
    assertEquals(SUCCESS, result.getStatus());

    commandList.clear();
    commandList.add(new ChangeCommand(15.0, -20.0, 5.0, Month.FEBRUARY.toString()));
    result = processor.process(commandList);
    assertNotNull(result);
    assertEquals(FAILURE, result.getStatus());
  }

  @Test
  void calculateBalance() {
    assertEquals(5400, equityBalance.getBalanceAmount());
    assertEquals(3150, debtBalance.getBalanceAmount());
    assertEquals(1200, goldBalance.getBalanceAmount());

    equityBalance = processor.calculateBalance(EQUITY, Month.FEBRUARY.toString(), 15.0);
    debtBalance = processor.calculateBalance(DEBT, Month.FEBRUARY.toString(), -20.0);
    goldBalance = processor.calculateBalance(GOLD, Month.FEBRUARY.toString(), 5.0);
    assertEquals(6785, equityBalance.getBalanceAmount());
    assertEquals(3320, debtBalance.getBalanceAmount());
    assertEquals(1522, goldBalance.getBalanceAmount());

    Mockito.when(balanceRepository.findByBalanceMonthAndFund(Month.MARCH.toString(), equityFund))
        .thenReturn(null);
    assertThrows(
        CommandValidatorException.class,
        () -> {
          processor.calculateBalance(EQUITY, Month.APRIL.toString(), 15.0);
        });

    List<Balance> balanceList = Arrays.asList(equityBalance, debtBalance, goldBalance);
    List<Rebalance> rebalanceList = processor.calculateRebalances(balanceList);
    assertEquals(6976, rebalanceList.get(0).getRebalanceAmount());
    assertEquals(3488, rebalanceList.get(1).getRebalanceAmount());
    assertEquals(1162, rebalanceList.get(2).getRebalanceAmount());
  }

  @Test
  void getType() {
    assertEquals(COMMAND_CHANGE, processor.getType());
  }
}
