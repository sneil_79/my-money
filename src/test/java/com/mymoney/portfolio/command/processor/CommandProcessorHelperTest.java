package com.mymoney.portfolio.command.processor;

import static com.mymoney.portfolio.common.Constants.CONTRIBUTION;
import static com.mymoney.portfolio.model.CommandResult.StatusEnum.SUCCESS;
import static org.junit.jupiter.api.Assertions.*;

import com.mymoney.portfolio.model.Command;
import com.mymoney.portfolio.model.CommandResult;
import java.util.*;
import org.junit.jupiter.api.Test;

class CommandProcessorHelperTest {

  String testOutput = "testOutput";

  @Test
  void processCommands() {
    CommandProcessor processor =
        new CommandProcessor() {
          @Override
          public CommandResult process(List commandList) {
            return new CommandResult(SUCCESS, null, testOutput);
          }

          @Override
          public String getType() {
            return CONTRIBUTION;
          }
        };
    Map<String, CommandProcessor> commandProcessorMap = new HashMap<>();
    commandProcessorMap.put(CONTRIBUTION, processor);
    CommandProcessorHelper helper =
        new CommandProcessorHelper(new CommandProcessorFactory(commandProcessorMap));

    Map<String, List<Command>> commandMap = new HashMap<>();
    commandMap.put(CONTRIBUTION, new ArrayList<>());

    assertEquals(testOutput, helper.processCommands(commandMap));
  }
}
