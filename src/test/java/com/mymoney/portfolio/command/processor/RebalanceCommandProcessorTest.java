package com.mymoney.portfolio.command.processor;

import static com.mymoney.portfolio.common.Constants.COMMAND_REBALANCE;
import static com.mymoney.portfolio.common.Constants.MSG_CANNOT_REBALANCE;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.MockitoAnnotations.initMocks;

import com.mymoney.portfolio.entity.Rebalance;
import com.mymoney.portfolio.entity.RebalanceRepository;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

class RebalanceCommandProcessorTest {

  @Mock RebalanceRepository rebalanceRepository;

  @InjectMocks RebalanceCommandProcessor processor;

  @BeforeEach
  void init_mocks() {
    initMocks(this);
  }

  @Test
  void process() {
    List<Rebalance> rebalances = new ArrayList<>();
    Mockito.when(rebalanceRepository.findAll()).thenReturn(rebalances);
    String output = processor.process(new ArrayList<>()).getOutput();
    assertTrue(output.contains(MSG_CANNOT_REBALANCE));

    rebalances.add(new Rebalance(null, Month.JUNE.toString(), 60L));
    rebalances.add(new Rebalance(null, Month.JUNE.toString(), 30L));
    rebalances.add(new Rebalance(null, Month.JUNE.toString(), 10L));

    output = processor.process(new ArrayList<>()).getOutput();
    assertTrue(output.contains(rebalances.get(0).getRebalanceAmount().toString()));
    assertTrue(output.contains(rebalances.get(1).getRebalanceAmount().toString()));
    assertTrue(output.contains(rebalances.get(2).getRebalanceAmount().toString()));

    rebalances.add(new Rebalance(null, Month.DECEMBER.toString(), 40L));
    rebalances.add(new Rebalance(null, Month.DECEMBER.toString(), 25L));
    rebalances.add(new Rebalance(null, Month.DECEMBER.toString(), 35L));

    output = processor.process(new ArrayList<>()).getOutput();
    assertFalse(output.contains(rebalances.get(0).getRebalanceAmount().toString()));
    assertFalse(output.contains(rebalances.get(1).getRebalanceAmount().toString()));
    assertFalse(output.contains(rebalances.get(2).getRebalanceAmount().toString()));
    assertTrue(output.contains(rebalances.get(3).getRebalanceAmount().toString()));
    assertTrue(output.contains(rebalances.get(4).getRebalanceAmount().toString()));
    assertTrue(output.contains(rebalances.get(5).getRebalanceAmount().toString()));
  }

  @Test
  void getType() {
    assertEquals(COMMAND_REBALANCE, processor.getType());
  }
}
