package com.mymoney.portfolio.command.processor;

import static com.mymoney.portfolio.common.Constants.*;
import static com.mymoney.portfolio.model.CommandResult.StatusEnum.SUCCESS;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.MockitoAnnotations.initMocks;

import com.mymoney.portfolio.entity.Fund;
import com.mymoney.portfolio.entity.FundRepository;
import com.mymoney.portfolio.model.CommandResult;
import com.mymoney.portfolio.model.ContributionCommand;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

class ContributionCommandProcessorTest {

  @Mock FundRepository fundRepository;

  @InjectMocks ContributionCommandProcessor processor;

  @BeforeEach
  void init_mocks() {
    initMocks(this);
    List<Fund> funds = new ArrayList<>();
    funds.add(new Fund(EQUITY, 60L, 60.0F, 10L));
    funds.add(new Fund(DEBT, 30L, 30.0F, 15L));
    funds.add(new Fund(EQUITY, 10L, 10.0F, 5L));
    Mockito.when(fundRepository.saveAll(ArgumentMatchers.any())).thenReturn(funds);
  }

  @Test
  void process() {
    List<ContributionCommand> input = new ArrayList<>();
    input.add(new ContributionCommand(COMMAND_ALLOCATE, 60L, 30L, 10L));
    input.add(new ContributionCommand(COMMAND_SIP, 10L, 15L, 5L));
    CommandResult result = processor.process(input);
    assertNotNull(result);
    assertEquals(SUCCESS, result.getStatus());
    assertEquals(3, ((List<Fund>)result.getData()).size());
  }

  @Test
  void getType() {
    assertEquals(CONTRIBUTION, processor.getType());
  }
}
