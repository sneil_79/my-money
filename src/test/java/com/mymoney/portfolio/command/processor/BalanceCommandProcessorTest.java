package com.mymoney.portfolio.command.processor;

import static com.mymoney.portfolio.common.Constants.*;
import static com.mymoney.portfolio.model.CommandResult.StatusEnum.SUCCESS;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.MockitoAnnotations.initMocks;

import com.mymoney.portfolio.entity.Balance;
import com.mymoney.portfolio.entity.BalanceRepository;
import com.mymoney.portfolio.entity.Fund;
import com.mymoney.portfolio.model.BalanceCommand;
import com.mymoney.portfolio.model.CommandResult;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

class BalanceCommandProcessorTest {

  @Mock BalanceRepository balanceRepository;

  @InjectMocks BalanceCommandProcessor processor;

  @BeforeEach
  void init_mocks() {
    initMocks(this);
  }

  @Test
  void process() {
    List<BalanceCommand> input = new ArrayList<>();

    List<Balance> monthBalances = new ArrayList<>();
    monthBalances.add(
        new Balance(new Fund(EQUITY, 6000L, 60.0F, 2000L), Month.JANUARY.toString(), 8000L));
    monthBalances.add(
        new Balance(new Fund(DEBT, 3000L, 30.0F, 500L), Month.JANUARY.toString(), 3500L));
    monthBalances.add(
        new Balance(new Fund(GOLD, 1000L, 10.0F, 1000L), Month.JANUARY.toString(), 2000L));
    input.add(new BalanceCommand(Month.JANUARY.toString()));
    Mockito.when(balanceRepository.findByBalanceMonth(Month.JANUARY.toString()))
        .thenReturn(monthBalances);

    monthBalances = new ArrayList<>();
    monthBalances.add(
        new Balance(new Fund(EQUITY, 6000L, 60.0F, 2000L), Month.JANUARY.toString(), 10000L));
    monthBalances.add(
        new Balance(new Fund(DEBT, 3000L, 30.0F, 500L), Month.JANUARY.toString(), 4000L));
    monthBalances.add(
        new Balance(new Fund(GOLD, 1000L, 10.0F, 1000L), Month.JANUARY.toString(), 3000L));
    input.add(new BalanceCommand(Month.FEBRUARY.toString()));
    Mockito.when(balanceRepository.findByBalanceMonth(Month.FEBRUARY.toString()))
        .thenReturn(monthBalances);

    CommandResult result = processor.process(input);
    assertNotNull(result);
    assertEquals(SUCCESS, result.getStatus());
    Map<String, List<Balance>> allBalances = (Map<String, List<Balance>>) result.getData();
    assertEquals(2, allBalances.size());
    assertNotNull(allBalances.get(Month.JANUARY.toString()));
    assertNotNull(allBalances.get(Month.FEBRUARY.toString()));
    assertNull(allBalances.get(Month.MARCH.toString()));
  }

  @Test
  void getType() {
    assertEquals(COMMAND_BALANCE, processor.getType());
  }
}
