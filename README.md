```mermaid
flowchart TD
    A[User] -->|Input File| B[File Reader] --> C{File exists, is TXT}
    C -- No --> D[End]
    C -- Yes --> E[Command Builder]
    E --> F{Is command format valid}
    F -- No --> D[End]
    F -- Yes --> G[Command Processor]
    G --> H[Allocate]
    H --> N{Success}
    N -- No --> D[End]
    N -- Yes --> I[SIP]
    I --> O{Success}
    O -- No --> D[End]
    O -- Yes --> J[CHANGE X months]
    J --> P{Success}
    P -- No --> D[End]
    P -- Yes --> K[BALANCE X months]
    K --> Q{Success}
    Q -- No --> D[End]
    Q -- Yes --> L[REBALANCE]
    L --> R{Success}
    R -- No --> D[End]
    R -- Yes --> M[Print Output] --> D[End]
```
